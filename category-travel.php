<?php get_header(); ?>

	<div class="container">
		<div class="col-lg-8 col-md-8">
		<?php
			while ( have_posts() ) :
				the_post();

				$subtitle = get_post_meta($post->ID, 'subtitle', true);
		?>
			<div class="travel-post">
				<div class="title"><?php the_title(); ?></div>
				<?php if( ! empty($subtitle) ): ?><div class="subtitle"><?php echo $subtitle; ?></div><?php endif; ?>
				<div class="date"><?php the_date('M. j Y'); ?></div>
				<?php if( has_post_thumbnail() ): ?>
				<?php
					$img_obj = wp_get_attachment_image_src( get_post_thumbnail_id(), 'list-thumb' );
				?>
				<div class="image" style="height:250px;background-image: url('<?php echo $img_obj[0]; ?>')"></div>
				<?php endif; ?>
				<div class="excerpt"><?php the_excerpt(); ?></div>
				<div class="readmore"><a href="<?php echo get_permalink(); ?>" class="readmore">Read More<i class="fa fa-angle-double-right" style="color:black;"></i></a></div>
			</div>
		<?php
			endwhile;
		?>
			<div class="readmore" style="text-align:right; margin-top:30px; width:90%; float:right; margin-right:60px;">
			<?php
				posts_nav_link( ' - ', 'Previous Page', 'Next Page' );
			?>
			</div>
		</div>

		<div class="col-lg-4 col-md-4 hidden-sm hidden-xs travel-sidebar" style="">
			<div class="about-irene">
				<div class="title">About Irene</div>
				<div class="content">
					<img src="http://1.bp.blogspot.com/-UcwJtS6L2TQ/UmvhMP6osyI/AAAAAAAAPgU/Kj6CuAZmlE8/s1600/DSC03791-002.JPG">
					<span class="excerpt">每年要去挑戰一個世界上沒有去過的地方，是我和老公的默契，隨著旅遊的經驗越來越多</span>
				</div>
			</div>

			<div class="popular-posts">
				<div class="title">Popular Posts</div>
				<div class="content">
					<img src="http://1.bp.blogspot.com/-UcwJtS6L2TQ/UmvhMP6osyI/AAAAAAAAPgU/Kj6CuAZmlE8/s1600/DSC03791-002.JPG">
					<span class="excerpt">每年要去挑戰一個世界上沒有去過的地方，是我和老公的默契，隨著旅遊的經驗越來越多</span>
				</div>
				<div class="content">
					<img src="http://1.bp.blogspot.com/-UcwJtS6L2TQ/UmvhMP6osyI/AAAAAAAAPgU/Kj6CuAZmlE8/s1600/DSC03791-002.JPG">
					<span class="excerpt">每年要去挑戰一個世界上沒有去過的地方，是我和老公的默契，隨著旅遊的經驗越來越多</span>
				</div>
				<div class="content">
					<img src="http://1.bp.blogspot.com/-UcwJtS6L2TQ/UmvhMP6osyI/AAAAAAAAPgU/Kj6CuAZmlE8/s1600/DSC03791-002.JPG">
					<span class="excerpt">每年要去挑戰一個世界上沒有去過的地方，是我和老公的默契，隨著旅遊的經驗越來越多</span>
				</div>
			</div>
			<div class="newest-posts">
				<div class="title">New Posts</div>
			</div>
		</div>

	</div>

<?php get_footer(); ?>
