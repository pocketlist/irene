	<?php
		$album_src = get_post_meta( $post->ID, 'album-src', true );
		if( !empty( $album_src) ):
			$album_ids = explode(',', $album_src);
	?>
	<div class="owl-carousel">
		<?php
			foreach( $album_ids as $img_id ):
				$img_obj = wp_get_attachment_image_src( $img_id, 'post-carousel' );
		?>
		<div class="item"><img src="<?php echo $img_obj[0]; ?>"></div>
		<?php endforeach; ?>
	</div>
	<script>
		jQuery(document).ready(function(){
			jQuery('.owl-carousel').owlCarousel({
			    margin:10,
			    items:3,
			    loop:true,
			    nav:false,
			    navRewind:false,
			    dots:true,
			    dotsEach:1
    		});
		});
	</script>
	<?php endif; ?>
	<div class="container single-post">
		<div class="single-post-title"><?php the_title(); ?></div>
		<div class="single-post-content">
			<p>
			<?php the_content(); ?>
			</p>

		</div>
		<div class="post-info">
			<span><?php the_date('M. j Y'); ?></span>
		</div>
	</div>
	<hr>
	<div class="container related-post">

	related post here
	</div>
