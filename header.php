<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<?php wp_head(); ?>
</head>
<body>
	<header class="container">
		<div>
			<div class="socialmediaicons">
				<?php
					$items = wp_get_nav_menu_items( 'socialmedia' );
					foreach ( (array) $items as $item) {
						echo '<a href="' . $item->url . '"><i class="' . implode(' ', $item->classes) . '"></i></a>';
					}
				?>
			</div>
			<div id="logo">
				<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/LOGO.png"  style="display: block; max-width:300px; height:auto; margin: 0 auto 5px auto;" alt="Irene's travel and living" title="Irene's travel and living"></a>
			</div>
		</div>

		<nav class="navbar ir">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-item">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="menu-item">
		      <ul class="nav navbar-nav top-nav">
				<?php
					$items = wp_get_nav_menu_items( 'main' );
					foreach ( (array) $items as $item) {
						echo '<li><a href="' . $item->url . '">' . $item->title . '</a></li>';
					}
				?>
		      </ul>
		    </div>
		    <!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</header>
