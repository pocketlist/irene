<?php


// Content meta fields
load_template( get_template_directory() . '/inc/content-metas.php' );


// album for post
load_template( get_template_directory() . '/inc/class-acme-footer-image.php' );
function run_acme_footer_image() {

    $plugin = new Acme_Footer_Image();
    $plugin->run();

}
run_acme_footer_image();


// set up custom meta for post: city-name, geo-lat, geo-lng
CustomMetaFactory::create( 'post', array(
    'subtitle'      => array(
        'title'     => 'Sub Title',
        'type'      => 'text',
        'context'   => 'normal',
    ),
    'city-name'      => array(
        'title'     => 'City Name',
        'type'      => 'text',
        'context'   => 'normal',
    ),
    'geo-lat'      => array(
        'title'     => 'GEO Latitude',
        'type'      => 'text',
        'context'   => 'side',
    ),
    'geo-lng'      => array(
        'title'     => 'GEO Longitude',
        'type'      => 'text',
        'context'   => 'side',
    ),
) );

// get geocode from google api
function grab_geocode( $post_id, $post, $update ) {

    // If this isn't a post, don't update it.
    if ( 'post' != $post->post_type ) {
        return;
    }

    $city_name = get_post_meta($post_id, 'city-name', true);
    $geo_lat = get_post_meta($post_id, 'geo-lat', true);
    $geo_lng = get_post_meta($post_id, 'geo-lng', true);
    if( empty( $city_name ) ){
        // clear geocode
        if( ! empty( $geo_lat ) ) delete_post_meta($post_id, 'geo-lat');
        if( ! empty( $geo_lng ) ) delete_post_meta($post_id, 'geo-lng');

        return; // return if city name is not available
    }

    if( ! empty( $geo_lat ) ) return; // return if geo-lat is available

    $result = wp_remote_get('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($city_name), array( 'timeout' => 30 ));

    if ( is_wp_error( $result ) ) {
        // add_post_meta($post_id, 'geo-lat', 'error', true);
        return; // return when failed to grab geocode
    }

    $the_body = wp_remote_retrieve_body($result);
    if ( is_wp_error( $the_body ) ) {
        // add_post_meta($post_id, 'geo-lat', 'error', true);
        return; // return when failed to grab geocode
    }

    $obj = json_decode($the_body);
    if( isset( $obj->results[0]->geometry->location ) ){
        $data = $obj->results[0]->geometry->location;
        if( isset( $data->lat ) and isset(  $data->lng ) ){
            add_post_meta($post_id, 'geo-lat', $data->lat, true);
            add_post_meta($post_id, 'geo-lng', $data->lng, true);
        } else {
            // add_post_meta($post_id, 'geo-lat', 'error', true);
            return; // return when failed to grab geocode
        }
    } else {
        // add_post_meta($post_id, 'geo-lat', 'error', true);
        return; // return when failed to grab geocode
    }

}  //end function
add_action( 'save_post', 'grab_geocode', 10, 3 );


function irene_setup() {

    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );

    register_nav_menus( array(
        'top-nav'       => 'Top Navigation',
        'sitemap'       => 'Site Map',
        'socialmedia'   => 'Social Media',
    ) );

    add_image_size( 'post-carousel', '450', '300' );
}
add_action( 'after_setup_theme', 'irene_setup' );

// setup scripts and styles
function irene_scripts() {


    // java script
    wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js', false);
    wp_enqueue_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js', array( 'jquery' ));
    wp_enqueue_script('google-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAfmHuib3aUBUvYUc8uqeaK9F1_lYI6tlA&sensor=true&language=zhtw', array( 'jquery' ));
    wp_enqueue_script('gmap-infobox', get_template_directory_uri() . '/js/infobox.js', array( 'jquery', 'google-map' ));
    wp_enqueue_script('mymap', get_template_directory_uri() . '/js/gmap.js', array( 'jquery', 'google-map' ));

    // style
    wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' );
    wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css' );
    wp_enqueue_style( 'bootstrap-theme', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css', array( 'bootstrap' ) );
    wp_enqueue_style( 'itl', get_template_directory_uri() . '/itl.css' );
    wp_enqueue_style( 'additional', get_template_directory_uri() . '/add.css' );
    wp_enqueue_style( 'google-font-quicksand', 'http://fonts.googleapis.com/css?family=Quicksand:300,400,700' );
    wp_enqueue_style( 'google-font-cardo', 'http://fonts.googleapis.com/css?family=Cardo:400,400italic,700' );

    // owl carousel
    if( is_single() ){
        wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css' );
        wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array( 'jquery' ), false, true);
    }
}
add_action( 'wp_enqueue_scripts', 'irene_scripts' );


// remove admin bar in frontend
add_filter('show_admin_bar', '__return_false');


// Automatically Remove Default Image Links
function wpb_imagelink_setup() {
    $image_set = get_option( 'image_default_link_type' );

    if ($image_set !== 'none') {
        update_option('image_default_link_type', 'none');
    }
}
add_action('admin_init', 'wpb_imagelink_setup', 10);

