jQuery(function ($) {
    function initialize() {
        var styles = [
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#fec89c"
                    }
                ]
            },{
                "featureType": "water",
                "stylers": [
                    { "color": "#ffffff" }
                ]
            },{
                "featureType": "administrative",
                "elementType": "geometry.all",
                "stylers": [
                    { "visibility": "off" }
                ]
            }
        ];

        var mapOptions = {
            zoom: 3,
            zoomControl: true,
            scrollwheel: true,
            streetViewControl: false,
            mapTypeControl: true,
            panControl: true,
            center: new google.maps.LatLng(25.0329636,121.5654268),
            mapTypeId: 'Styled'
        }
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        var styledMapType = new google.maps.StyledMapType(styles, { name: 'Styled' });
        map.mapTypes.set('Styled', styledMapType);

        setMarkers(map);
    }

    /**
     * Data for the markers consisting of a name, a LatLng and a zIndex for
     * the order in which these markers should display on top of each
     * other.
     */

    function setMarkers(map) {

        var image = '/wp-content/themes/irene/img/itl_dot.png';
        $.ajax({
            url: "tj",
            dataType: "json",
            type: "GET",
            ajaxStart: function () {},
            error: function (xhr) {
                alert('Ajax request 發生錯誤');
            },
            success: function (json) {
                var NumOfData = json.length;

                for (var i = 0; i < json.length; i++) {
                    var position = new google.maps.LatLng(json[i]["lat"], json[i]["lng"]);
                    var marker = new google.maps.Marker({
                        position: position,
                        icon: image,
                        title: json[i]["title"],
                        map: map
                    });
                    attachSecretMessage(marker, json[i]);
                }
            }
        });

    }

    function attachSecretMessage(marker, obj) {
        var infowindow = new InfoBox({
            content: "<h4><a href=\"" + obj['link'] + "\">" + obj["title"] + "</h4>",
            boxClass: "myInfobox",
            pane: "floatPane"
        });
        google.maps.event.addListener(marker, 'mouseover', function () {
            infowindow.open(marker.get('map'), marker);
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);

});