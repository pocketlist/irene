<?php
/**
 * The template for displaying the footer
 *
 */
?>

	<hr>
	<footer class="container">
		<div class="sitemap">
			<div class="title">Irene's Travel & Living</div>
			<ul class="footer-ul">
				<?php
					$items = wp_get_nav_menu_items( 'main' );
					foreach ( (array) $items as $item) {
						echo '<li><a href="' . $item->url . '">' . $item->title . '</a></li>';
					}
				?>
		    </ul>
		</div>
		<div class="socialmedia">
			<div class="title">Follow Me</div>
			<ul class="footer-ul">
				<?php
					$items = wp_get_nav_menu_items( 'socialmedia' );
					foreach ( (array) $items as $item) {
						echo '<li><a href="' . $item->url . '">' . $item->title . '</a></li>';
					}
				?>
		    </ul>
		</div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>
