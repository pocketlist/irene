<?php

/**
 * Custom post meta fields
 */


/**
 * Class HugoCustomMetaFactory
 */
class CustomMetaFactory {
	public static function create( $type, $metas ) {
		return new CustomMetas( $type, $metas );
	}
}

/**
 * Class HugoCustomMetas
 */
class CustomMetas {

	private $metas = array(
		'normal' => array(),
		'side' => array()
	);
	private $type = NULL;

	function __construct( $type, $metas ) {

		// Saving global $post variable
		global $post;
		$this->post = & $post;

		// Setting post type
		$this->type  = $type;

		// Determine if we need normal and side boxes
		foreach ( $metas as $meta_id => $meta_data ) {
			$this->metas[ $meta_data['context'] ][ $meta_id ] = $metas[ $meta_id ];
		}

		// Using correct hooks to add functionality
		// add_action( 'is_protected_meta', array( $this, 'protect' ) );
		add_action( 'admin_menu', array( $this, 'add' ) );
		add_action( 'save_post', array( $this, 'save' ) );
	}

	/**
	 * Hide core post meta fields
	 *
	 * @param $protected
	 * @param $meta_key
	 *
	 * @return bool
	 */
	function protect ( $protected, $meta_key ) {
		return in_array( $meta_key, array_keys( $this->metas ) ) ? TRUE : $protected;
	}

	/**
	 * Attempts to add the meta box to the admin view.
	 * Returns true on success, false on failure
	 *
	 * @return bool
	 */
	function add() {

		if ( ! empty( $this->metas['normal'] ) ) {
			add_meta_box(
				'wprankit-meta-boxes-normal',
				'Additional Field',
				array( $this, 'fields_normal' ),
				$this->type,
				'normal',
				'high'
			);
		}

		if ( ! empty( $this->metas['side'] ) ) {
			add_meta_box(
				'wprankit-meta-boxes-side',
				'GEO Code',
				array( $this, 'fields_side' ),
				$this->type,
				'side',
				'default'
			);
		}
	}

	/**
	 * Display the custom meta fields in the 'normal' context
	 */
	function fields_normal() {

		// How many fields are we displaying? Used to check if no fields are shown
		$field_count = 0;

		echo '<table class="form-table">';

		foreach ( $this->metas['normal'] as $meta_key => $meta ) :

			$field_insert = '';

			// Are we on the right page template?
			if ( ! empty( $meta['template'] ) ) {
				$template = get_post_meta( $this->post->ID, '_wp_page_template', TRUE );
				if ( $template !== $meta['template'] )
					continue;
			}

			if ( ! empty( $meta['disabled'] ) ) {
				$field_insert .= ' disabled';
			}

			// Get the current data stored for this field, if any
			$curr_value = get_post_meta( $this->post->ID, $meta_key, TRUE );

			if ( empty( $curr_value ) && isset( $meta['default'] ) ) {
				$curr_value = $meta['default'];
			}

			echo '
					<tr>
						<th scope="row">
							<label for="' . $meta_key . '">' . $meta['title'] . '</label>
						</th>
						<td>
							<input type="hidden" name="' . $meta_key . '_noncename" id="' . $meta_key . '_noncename" value="' . wp_create_nonce( 'wp-rank-it' ) . '">';

			switch ( $meta['type'] ) :

				case 'text':
				case 'number':
				case 'email':
				case 'url':
				case 'password':
					echo '
					<input type="' . $meta['type'] . '" name="' . $meta_key . '_value" id="' . $meta_key . '" value="' .
							$curr_value . '" class="large-text"' . $field_insert . '>';
					break;

				// Converts timecode to date on output
				case 'date':
					echo '
					<input type="text" name="' . $meta_key . '_value" id="' . $meta_key . '" value="' .
							( ! empty( $curr_value ) ? date( 'n/j/Y', $curr_value ) : '' ) . '" class="large-text"' . $field_insert . '>';
					break;

				// Alternate title saving field
				case 'post_title':
					echo '
					<input type="text" name="post_title" id="title" value="' . $this->post->post_title . '" >';
					break;

				case 'textarea':
					echo '
					<textarea name="' . $meta_key . '_value" id="' . $meta_key . '" cols="40" rows="4" class="large-text"' .
							$field_insert . '>' . $curr_value . '</textarea>';
					break;

				case 'checkbox':

					// Allows for a callback for checkbox options
					if ( isset( $meta['options'] ) && ! is_array( $meta['options'] ) && function_exists( $meta['options'] ) ) {
						$meta['options'] = $meta['options']();
					}

					// Single checkbox, yes/no
					if ( empty( $meta['options'] ) || ! is_array( $meta['options'] ) ) {
						echo '<input type="checkbox" name="' . $meta_key . '_value" id="' . $meta_key . '" value="1"';
						echo( $curr_value == '1' ? 'checked="checked"' : '' );
						echo '>';

						// Multiple checkboxes
					}
					else {
						foreach ( $meta['options'] as $opv ) {
							echo '<input type="checkbox" name="' . $meta_key . '_value[]" id="' .
									$meta_key . '" value="' . $opv . '"';

							if ( ! empty( $curr_value ) && in_array( $opv, $curr_value ) )
								echo ' checked="checked"';

							echo '> <span>' . $opv . '</span> &nbsp;&nbsp;';
						}
					}
					break;

				case 'select':
				case 'select_assoc':

					// Allows for a callback for select options
					if ( ! is_array( $meta['options'] ) && function_exists( $meta['options'] ) ) {
						$meta['options'] = $meta['options']();
					}

					echo '<select name="' . $meta_key . '_value" id="' . $meta_key . '">';

					foreach ( $meta['options'] as $opk => $opv ) :
						// If this is not an associative array, set the key to the value
						$opk = ( $meta['type'] == 'select_assoc' ? $opk : $opv );
						echo '<option value="' . $opk . '"';
						if ( $opk == $curr_value )
							echo ' selected';
						echo '>' . $opv . '</option>';
					endforeach;

					echo '</select>';
					break;

				case 'radio':

					// Allows for a callback for select options
					if ( ! is_array( $meta['options'] ) && function_exists( $meta['options'] ) ) {
						$meta['options'] = $meta['options']();
					}

					if ( empty( $meta['options'] ) || ! is_array( $meta['options'] ) ) {
						continue;
					}

					foreach ( $meta['options'] as $opk => $opv ) {
						echo '<input type="radio" name="' . $meta_key . '_value" id="' . $meta_key . '_' . $opk . '" value="' . $opk . '"';
						echo( $opk == $curr_value ? 'checked="checked"' : '' );
						echo '/> <label for="' . $opk . '">' . $opv . '</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					}
					break;

				case 'html':
					break;

			endswitch;

			if ( $meta['type'] === 'url' && ! empty( $curr_value ) ) {
				printf(
					'<p class="description"><a href="%s">%s</a></p>',
					esc_url( $curr_value ),
					__( 'Go to this link &rarr;', 'wp-rank-it' )
				);
			}

			if ( ! empty( $meta['description'] ) ) {
				echo '<p class="description">' . $meta['description'] . '</p>';
			}

			echo '
			</td></tr>';

			$field_count ++;

		endforeach;

		echo '</table>';

		if ( $field_count === 0 ) {
			echo '<p><em>No additional content fields for this page.</em></p>';
		}

	}

	/**
	 * Display the custom data fields in the 'side' context
	 */
	function fields_side() {

		echo '<table class="form-table">';

		foreach ( $this->metas['side'] as $meta_key => $meta ) :

			// Are we on the right page template?
			if ( ! empty( $meta['template'] ) ) {
				$template = get_post_meta( $this->post->ID, '_wp_page_template', TRUE );
				if ( $template !== $meta['template'] )
					continue;
			}

			// Get the current data stored for this field, if any
			$curr_value = get_post_meta( $this->post->ID, $meta_key, TRUE );

			printf(
				'<tr><th scope="row"><label>%s</label> <span class="description">%s</span></th><td>
					<span class="data">%s</span></td></tr>',
				$meta['title'],
				! empty( $meta['description'] ) ? $meta['description'] : '',
				$curr_value
			);

		endforeach;

		echo '</table>';
	}

	/**
	 * Save the various custom meta fields
	 */
	function save() {

		if (
			// Nothing to do without POST info
			empty( $_POST ) ||
			// Quick edit saves
			( isset( $_POST['action'] ) && $_POST['action'] == 'inline-save' ) ||
			// No post type to check
			! isset( $_POST['post_type'] ) ||
			// A different post type than the fields we're saving
			$_POST['post_type'] != $this->type
		) {
			return;
		}

		// Checking auth
		if ( ! current_user_can( 'edit_post', $this->post->ID ) ) {
			return;
		}

		// Only iterate through the normal context fields
		foreach ( $this->metas['normal'] as $meta_key => $meta ) :

			// Don't do anything if this does not match the template for this meta
			if (
				! empty( $meta['template'] ) &&
				( empty( $_POST['page_template'] ) || $_POST['page_template'] != $meta['template'] )
			) {
				continue;
			}

			// This field type does not need to be saved
			if ( $meta['type'] == 'html' ) {
				continue;
			}

			// Verify field-specific nonce fields
			if ( ! wp_verify_nonce( $_POST[$meta_key . '_noncename'], 'wp-rank-it' ) ) {
				continue;
			}



			// If the value is set, set the meta field
			if ( ! empty( $_POST[ $meta_key . '_value' ] ) ) {
				$new_data = $_POST[$meta_key . '_value'];

				if ( $meta['type'] === 'date' && ! empty( $new_data ) ) {
					$new_data = strtotime( $new_data );

					if ( ! $new_data ) {
						continue;
					}
				}

				$curr_data = get_post_meta( $this->post->ID, $meta_key, TRUE );

				if ( empty( $new_data ) )
					delete_post_meta( $this->post->ID, $meta_key );

				elseif ( $new_data != $curr_data )
					update_post_meta( $this->post->ID, $meta_key, $new_data );
			}
			// Otherwise, delete the meta field
			else {
				delete_post_meta( $this->post->ID, $meta_key );
			}

		endforeach;
	}
}