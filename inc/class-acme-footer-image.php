<?php

/**
 * The dashboard-specific functionality of the plugin.
 *
 * @link       http://tommcfarlin.com
 * @since      0.1.0
 *
 * @package    Acme_Footer_Image
 * @subpackage Acme_Footer_Image/admin
 */

/**
 * The dashboard-specific functionality of the plugin.
 *
 * Defines the plugin name, version, the meta box functionality
 * and the JavaScript for loading the Media Uploader.
 *
 * @package    Acme_Footer_Image
 * @subpackage Acme_Footer_Image/admin
 * @author     Tom McFarlin <tom@tommcfarlin.com>
 */
class Acme_Footer_Image {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string    $name    The ID of this plugin.
	 */
	private $name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string    $version    The version of the plugin
	 */
	private $version;

	/**
	 * Initializes the plugin by defining the properties.
	 *
	 * @since 0.1.0
	 */
	public function __construct() {

		$this->name = 'acme-footer-image';
		$this->version = '1.0.0';

	}

	/**
	 * Defines the hooks that will register and enqueue the JavaScriot
	 * and the meta box that will render the option.
	 *
	 * @since 0.1.0
	 */
	public function run() {

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
		add_action( 'save_post', array( $this, 'save_post' ) );

	}

	/**
	 * Renders the meta box on the post and pages.
	 *
	 * @since 0.1.0
	 */
	public function add_meta_box() {

		$screens = array( 'post' );

		foreach ( $screens as $screen ) {

			add_meta_box(
				$this->name,
				'Album',
				array( $this, 'display_featured_footer_image' ),
				$screen,
				'side'
			);

		}

	}

	/**
	 * Registers the JavaScript for handling the media uploader.
	 *
	 * @since 0.1.0
	 */
	public function enqueue_scripts() {

		if( is_admin() ){
			wp_enqueue_script(
				$this->name,
				get_template_directory_uri() . '/js/post_album.js',
				array( 'jquery' ),
				$this->version,
				'all'
			);
		}
	}

	/**
	 * Sanitized and saves the post featured footer image meta data specific with this post.
	 *
	 * @param    int    $post_id    The ID of the post with which we're currently working.
	 * @since    1.0.0
	 */
	public function save_post( $post_id ) {

		if ( isset( $_REQUEST['album-src'] ) ) {
			update_post_meta( $post_id, 'album-src', sanitize_text_field( $_REQUEST['album-src'] ) );
		}

	}

	/**
	 * Renders the view that displays the contents for the meta box that for triggering
	 * the meta box.
	 *
	 * @param    WP_Post    $post    The post object
	 * @since    0.1.0
	 */
	public function display_featured_footer_image( $post ) {
?>
<style>#featured-footer-image-container img {width:100%;height:auto;}</style>
<p class="hide-if-no-js">
	<a title="Set Footer Image" href="javascript:;" id="set-footer-thumbnail" class="button">Set album</a>
</p>

<div id="featured-footer-image-container" class="hidden">
<?php
	$album_src = get_post_meta( $post->ID, 'album-src', true );
	if( ! empty( $album_src ) ){
		$ids = explode( ',', $album_src );
		foreach( $ids as $image_id ){
			$img = wp_get_attachment_image( $image_id, 'large' );
			echo $img;
			// echo '<img src="' . $img->src . '"><br>';
		}
	}
?>
</div><!-- #featured-footer-image-container -->

<p class="hide-if-no-js hidden">
	<a title="Remove Footer Image" href="javascript:;" id="remove-footer-thumbnail" class="button">Remove album</a>
</p>
	<input type="hidden" id="album-src" name="album-src" value="<?php echo get_post_meta( $post->ID, 'album-src', true ); ?>" />

<?php
	}

}
