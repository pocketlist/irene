<?php get_header(); ?>

	<div class="category-container container">
		<?php if ( have_posts() ) : ?>

		<div class="col-lg-2 visible-lg">
		<?php
			if( is_category() ):

				$thisCat = get_category( get_query_var( 'cat' ) );
				// $cat_id = $category->cat_ID;

				if( $thisCat->slug == 'city-guide' ) {
					$sub_cat_title = 'CITY';
				} else {
					$sub_cat_title = 'SHOW ME';
				}

				$sub_cat_args = array(
						'type'        => 'post',
						'child_of'    => $thisCat->term_id,
						'parent'      => '',
						'orderby'     => 'name',
						'order'       => 'ASC',
						'hide_empty'  => 0,
						'hierarchical'=> 1,
						'exclude'     => '',
						'include'     => '',
						'number'      => '',
						'taxonomy'    => 'category',
						'pad_counts'  => false
					);

				$sub_cat = get_categories( $sub_cat_args );

				if( ! empty( $sub_cat ) ):
		?>
			<div class="category-filter">
				<div class="title"><?php echo $sub_cat_title; ?><i class="fa fa-angle-down"></i></div>
				<ul>
				<?php foreach( $sub_cat as $sc ): ?>
				    <li><a href="<?php echo get_category_link( $sc->term_id ); ?>"><?php echo $sc->name; ?></a></li>
				<?php endforeach; ?>
			    </ul>
			</div>
		<?php
				endif;
			endif;
		?>
		</div>

		<div class="col-lg-10 col-md-12">
			<div class="row">
			<?php

			while ( have_posts() ) :
				the_post();
				// print_r(array_keys(get_object_vars($post)));
			?>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 category-post-div">
					<?php if( has_post_thumbnail() ): ?>
					<?php
						$img_obj = wp_get_attachment_image_src( get_post_thumbnail_id(), 'list-thumb' );
					?>
					<div class="post-image" style="background-image: url('<?php echo $img_obj[0]; ?>');"></div>
					<?php endif; ?>
					<div class="post-info">
						<span><?php the_date('M. j'); ?></span>
						&nbsp;/&nbsp;
						<span>
						<?php
							$categories = get_the_category($post->ID);
							echo $categories[0]->name;
						?>
						</span>
					</div>
					<div class="post-title"><?php the_title(); ?></div>
					<div class="post-excerpt"><?php echo mb_substr(get_the_excerpt(), 0, 24); ?></div>
					<div><a href="<?php echo get_permalink(); ?>" class="readmore">Read More<i class="fa fa-angle-double-right" style="color:black;"></i></a></div>
				</div>
			<?php
			endwhile;
			?>
			</div>
		</div>

		<div class="readmore" style="text-align:right; margin-top:30px; width:90%; float:right; margin-right:60px;">
		<?php
			posts_nav_link( ' - ', 'Previous Page', 'Next Page' );
		?>
		</div>

		<?php endif; ?>

	</div>


<?php get_footer(); ?>
