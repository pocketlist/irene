<?php
/*
    Template Name: travel city json
*/
$output = array();

$args = array(
	'posts_per_page'   => 10,
	'orderby'          => 'post_date',
	'order'            => 'DESC',
	'post_type'        => 'post',
	'post_status'      => 'publish',
	'meta_query' => array(
		'relation' => 'AND',
		array(
			'key'     => 'geo-lat',
			'value'   => ' ',
			'compare' => '!=',
		),
		array(
			'key'     => 'geo-lng',
			'value'   => ' ',
			'compare' => '!=',
		),
	),
);

$posts_array = get_posts( $args );
foreach( $posts_array as $post ){
	$post_id = $post->ID;
    $geo_lat = get_post_meta($post_id, 'geo-lat', true);
    $geo_lng = get_post_meta($post_id, 'geo-lng', true);

	$output[] = array(
		'title'	=> $post->post_title,
		'link'	=> get_permalink($post_id),
		'lat' 	=> $geo_lat,
		'lng' 	=> $geo_lng,
		);
}

wp_send_json($output);